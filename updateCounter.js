const totalItems = 4;

updateCounter = function(){
    // Increment the counter
    this.counter++;

    // Make sure that the counter doesn't get to high (between 0 and 3 in this case).
    this.counter %= totalItems;
}